# Changelog

## Version 0.3.0

### New features

  * non-empty folders get imported as LDP containers
  * non-existing resources now return 404 instead of an empty document

## Version 0.2.0

### New features

  * start and stop Molid programmatically
  * use Molid for integration testing
  * Jest support via givenMolid()

## Version 0.1.2

### New features

  * Start via npx or via npm script
  * Initialize with data from local .ttl files
  * Serve data as text/turtle via http