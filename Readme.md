Molid - Mock Solid Server
=========================

A mock server, that can be used for testing Solid apps locally.

Use cases: 

  * Provide data on localhost during development 🚧
  * Ensure your Solid app is really compatible to different kind of Pod structures 🔗
  * Provision Test-Pods for automated integration testing 🚦
  * ...     
  
[![Documentation Status](https://readthedocs.org/projects/molid/badge/?version=latest)](https://molid.readthedocs.io/en/latest/?badge=latest)

## Quick start 🚀

Start using `npx`, no installation needed

```bash
$ npx molid
```

> Molid - mock Solid server listening on port 3333!

Molid provides some Solid data, that you can now access via HTTP, e.g.

```bash
$ curl http://localhost:3333/profile/card
```

All the mock data is stored within the `.molid` folder in your current directory. Adjust it to your needs and restart.

## Read The Docs 📖

Read the whole documentation on https://molid.readthedocs.io