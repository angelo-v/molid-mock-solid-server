import fs from 'fs';
import { ncp } from 'ncp';

ncp.limit = 16;

export const exists = path => fs.existsSync(path);

export const read = path => fs.readFileSync(path, 'utf8');

export const copy = (from, to) => {
    return new Promise((resolve, reject) => {
        ncp(from, to, function(err) {
            if (err) {
                reject(err);
            }
            resolve();
        });
    });
};
