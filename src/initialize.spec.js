import { copy, exists } from './file-system';
import initialize from './initialize';

jest.mock('./file-system');
jest.mock('./config', () => ({
    initialDataDir: '/initial/data/dir',
}));

describe('initialize data directory', () => {
    beforeEach(() => {
        jest.resetAllMocks();
        copy.mockReturnValue(Promise.resolve());
    });

    it('copies initial files to data dir', () => {
        exists.mockReturnValue(false);
        initialize('.test-data-dir');
        expect(copy).toHaveBeenCalledWith(
            '/initial/data/dir',
            '.test-data-dir'
        );
        expect(exists).toHaveBeenCalledWith('.test-data-dir');
    });

    it('resolves to the data dir after copying files', async () => {
        exists.mockReturnValue(false);
        const result = await initialize('.test-data-dir');
        expect(result).toBe('.test-data-dir');
    });

    it('rejects when copying fails', async () => {
        exists.mockReturnValue(false);
        copy.mockReturnValue(Promise.reject(new Error('test')));
        const result = initialize('.test-data-dir');
        await expect(result).rejects.toEqual(new Error('test'));
    });

    it('does not copy to data dir if it already exists', () => {
        exists.mockReturnValue(true);
        initialize('.test-data-dir');
        expect(copy).not.toHaveBeenCalled();
        expect(exists).toHaveBeenCalledWith('.test-data-dir');
    });

    it('resolves to the data dir when data dir already exists', async () => {
        exists.mockReturnValue(true);
        const result = await initialize('.test-data-dir');
        expect(result).toBe('.test-data-dir');
    });
});
