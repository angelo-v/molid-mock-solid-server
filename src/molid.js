#!/usr/bin/env node

import { start } from './index';

const DEFAULT_PORT = 3333;

start({
    port: process.env.PORT || DEFAULT_PORT,
});
