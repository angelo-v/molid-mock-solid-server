import path from 'path';
import sanitize from 'sanitize-filename';
import { start } from '../index';

export function givenMolid(description, tests) {
    describe(`given molid ${description}`, () => {
        let dataDir = path.resolve(
            module.parent.filename,
            `../${sanitize(description)}.molid`
        );
        const molidFixture = {};
        beforeAll(async () => {
            const molid = await start({
                dataDir,
            });
            Object.assign(molidFixture, molid);
        });

        afterAll(async () => {
            await molidFixture.stop();
        });
        tests(molidFixture);
    });
}
