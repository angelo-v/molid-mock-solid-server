import importData from './import';
import initialize from './initialize';
import { startServer } from './server';

import { defaultDataDir } from './config';

export const start = ({ port, dataDir = defaultDataDir } = {}) =>
    initialize(dataDir)
        .then(importData)
        .then(store => startServer(store, port))
        .catch(err => console.log(err));
