import createContainer from './createContainer';
import getContainer from './getContainer';

export default function importContainers(urlPath, store) {
    const container = getContainer(urlPath);
    console.log('adding', urlPath, 'to container', container.path);
    createContainer(store, container.path, urlPath);
    if (container.depth > 0) {
        importContainers(container.path, store);
    }
}
