import { rdfBaseUri } from '../../config';
import { sym } from 'rdflib';

export default (store, container, content) => {
    addType(store, container, 'Container');
    addType(store, container, 'BasicContainer');
    store.add(
        sym(`${rdfBaseUri}${container}`),
        sym('http://www.w3.org/ns/ldp#contains'),
        sym(`${rdfBaseUri}${content}`),
        sym(`${rdfBaseUri}${container}`)
    );
};

function addType(store, container, containerType = 'Container') {
    store.add(
        sym(`${rdfBaseUri}${container}`),
        sym('http://www.w3.org/1999/02/22-rdf-syntax-ns#type'),
        sym(`http://www.w3.org/ns/ldp#${containerType}`),
        sym(`${rdfBaseUri}${container}`)
    );
}
