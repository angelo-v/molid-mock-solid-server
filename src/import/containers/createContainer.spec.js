import { graph, sym } from 'rdflib';
import createContainer from './createContainer';

jest.mock('../../config', () => ({
    rdfBaseUri: 'http://test.example',
}));

describe('create container', () => {
    let store;
    beforeEach(() => {
        store = graph();
    });
    describe('of a file in a top level container', () => {
        it('adds contains predicate for the conent', () => {
            createContainer(store, '/container/', '/container/content');
            expect(
                store.match(
                    null,
                    sym('http://www.w3.org/ns/ldp#contains'),
                    null
                )
            ).toEqual([
                {
                    subject: {
                        termType: 'NamedNode',
                        value: 'http://test.example/container/',
                    },
                    predicate: {
                        termType: 'NamedNode',
                        value: 'http://www.w3.org/ns/ldp#contains',
                    },
                    object: {
                        termType: 'NamedNode',
                        value: 'http://test.example/container/content',
                    },
                    why: {
                        termType: 'NamedNode',
                        value: 'http://test.example/container/',
                    },
                },
            ]);
        });

        it('adds Container types', () => {
            createContainer(store, '/container/', '/container/content');
            expect(
                store.match(
                    null,
                    sym('http://www.w3.org/1999/02/22-rdf-syntax-ns#type'),
                    null
                )
            ).toEqual([
                {
                    subject: {
                        termType: 'NamedNode',
                        value: 'http://test.example/container/',
                    },
                    predicate: {
                        termType: 'NamedNode',
                        value:
                            'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
                    },
                    object: {
                        termType: 'NamedNode',
                        value: 'http://www.w3.org/ns/ldp#Container',
                    },
                    why: {
                        termType: 'NamedNode',
                        value: 'http://test.example/container/',
                    },
                },
                {
                    subject: {
                        termType: 'NamedNode',
                        value: 'http://test.example/container/',
                    },
                    predicate: {
                        termType: 'NamedNode',
                        value:
                            'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
                    },
                    object: {
                        termType: 'NamedNode',
                        value: 'http://www.w3.org/ns/ldp#BasicContainer',
                    },
                    why: {
                        termType: 'NamedNode',
                        value: 'http://test.example/container/',
                    },
                },
            ]);
        });
    });

    describe('of a file in a second level container', () => {
        it('adds contains predicate for the file to its container', () => {
            createContainer(store, '/first/second/', '/first/second/file');
            expect(
                store.match(
                    null,
                    sym('http://www.w3.org/ns/ldp#contains'),
                    null
                )
            ).toEqual([
                {
                    subject: {
                        termType: 'NamedNode',
                        value: 'http://test.example/first/second/',
                    },
                    predicate: {
                        termType: 'NamedNode',
                        value: 'http://www.w3.org/ns/ldp#contains',
                    },
                    object: {
                        termType: 'NamedNode',
                        value: 'http://test.example/first/second/file',
                    },
                    why: {
                        termType: 'NamedNode',
                        value: 'http://test.example/first/second/',
                    },
                },
            ]);
        });
    });
});
