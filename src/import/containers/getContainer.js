const trailingSlash = /\/$/;

export default containerFile => {
    const segments = containerFile.replace(trailingSlash, '').split('/');
    segments.pop();
    const container = segments.join('/') + '/';
    return {
        path: container,
        depth: segments.length - 1,
    };
};
