import { graph } from 'rdflib';
import importContainers from './importContainers';
import createContainer from './createContainer';

jest.mock('../../config', () => ({
    rdfBaseUri: 'http://test.example',
}));

jest.mock('./createContainer');

describe('import containers', () => {
    let store;
    beforeEach(() => {
        store = graph();
    });
    it('creates first level container with content', () => {
        importContainers('/fake/path', store);
        expect(createContainer).toHaveBeenCalledWith(
            store,
            '/fake/',
            '/fake/path'
        );
    });

    it('creates second level container with content', () => {
        importContainers('/first/second/file', store);
        expect(createContainer).toHaveBeenCalledWith(
            store,
            '/first/second/',
            '/first/second/file'
        );
    });

    it('creates first level container with second level container as content', () => {
        importContainers('/first/second/file', store);
        expect(createContainer).toHaveBeenCalledWith(
            store,
            '/first/',
            '/first/second/'
        );
    });
});
