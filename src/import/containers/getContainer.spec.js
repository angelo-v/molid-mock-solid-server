import getContainer from './getContainer';

describe('get container', () => {
    it('of file in root', () => {
        const { path, depth } = getContainer('/file');
        expect(path).toBe('/');
        expect(depth).toBe(0);
    });

    it('of file in first level container', () => {
        const { path, depth } = getContainer('/container/file');
        expect(path).toBe('/container/');
        expect(depth).toBe(1);
    });

    it('of file in second level container', () => {
        const { path, depth } = getContainer('/first/second/file');
        expect(path).toBe('/first/second/');
        expect(depth).toBe(2);
    });

    it('of a sub container', () => {
        const { path, depth } = getContainer('/first/second/');
        expect(path).toBe('/first/');
        expect(depth).toBe(1);
    });

    it('of a third level container', () => {
        const { path, depth } = getContainer('/first/second/third/');
        expect(path).toBe('/first/second/');
        expect(depth).toBe(2);
    });
});
