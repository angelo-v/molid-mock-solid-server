import getUrlPath from './getUrlPath';

describe('getUrlPath', () => {
    it('should match the file path', () => {
        expect(getUrlPath('/some/file')).toBe('/some/file');
    });
    it('should add leading slash to url path', () => {
        expect(getUrlPath('some/file')).toBe('/some/file');
    });
    it('should match the file path with ending', () => {
        expect(getUrlPath('/some/file.ttl')).toBe('/some/file.ttl');
    });
    it('should exclude $.ttl file ending in url path', () => {
        expect(getUrlPath('/some/file$.ttl')).toBe('/some/file');
    });
});
