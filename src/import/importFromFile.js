import getUrlPath from './getUrlPath';
import { parse } from 'rdflib';
import { rdfBaseUri } from '../config';
import { read as readFile } from '../file-system';
import { importContainers } from './containers';

export default (entry, store) => {
    const urlPath = getUrlPath(entry.path);
    console.log('importing', entry.path, 'to', urlPath);
    const data = readFile(entry.fullPath);
    parse(data, store, `${rdfBaseUri}${urlPath}`, 'text/turtle');
    importContainers(urlPath, store);
};
