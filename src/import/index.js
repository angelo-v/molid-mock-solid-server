import { graph } from 'rdflib';
import importFromFile from './importFromFile';
import readdirp from 'readdirp';

export default srcDir => {
    const problems = [];
    const store = graph();
    return new Promise((resolve, reject) => {
        readdirp(srcDir, {
            entryType: 'files',
        })
            .on('data', function(entry) {
                importFromFile(entry, store);
            })
            .on('warn', function(warn) {
                problems.push(warn);
            })
            .on('error', function(err) {
                problems.push(err);
            })
            .on('end', function() {
                if (problems.length > 0) {
                    console.error('data import failed');
                    reject(problems);
                } else {
                    console.log('finished data import');
                    resolve(store);
                }
            });
    });
};
