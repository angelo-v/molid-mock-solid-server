import path from 'path';

export default filePath => {
    const absolutePath = path.join('/', filePath);
    const stripEnding = '$.ttl';
    return absolutePath.endsWith(stripEnding)
        ? absolutePath.substr(0, absolutePath.indexOf(stripEnding))
        : absolutePath;
};
