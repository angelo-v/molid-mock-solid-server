import { graph } from 'rdflib';
import importFromFile from './importFromFile';
import { read as readFile } from '../file-system';
import { importContainers } from './containers';

jest.mock('../file-system');
jest.mock('../config', () => ({
    rdfBaseUri: 'http://test.example',
}));

jest.mock('./containers/importContainers');

describe('import from file', () => {
    let store;
    beforeEach(() => {
        store = graph();
    });

    it('should add triples from file to the store', () => {
        readFile.mockReturnValue(
            '<http://some.example/subject> <http://some.example/predicate> "Literal".'
        );
        const entry = {
            path: 'fake/path',
            fullPath: '/full/fake/path',
        };
        importFromFile(entry, store);
        expect(store.match()).toEqual([
            {
                object: {
                    termType: 'Literal',
                    value: 'Literal',
                },
                predicate: {
                    termType: 'NamedNode',
                    value: 'http://some.example/predicate',
                },
                subject: {
                    termType: 'NamedNode',
                    value: 'http://some.example/subject',
                },
                why: {
                    termType: 'NamedNode',
                    value: 'http://test.example/fake/path',
                },
            },
        ]);
    });

    it('should add triples with URIs relative to document', () => {
        readFile.mockReturnValue('<#me> <../vocab/predicate> <other#it>.');
        const entry = {
            path: 'fake/path',
            fullPath: '/full/fake/path',
        };
        importFromFile(entry, store);
        expect(store.match()).toEqual([
            {
                object: {
                    termType: 'NamedNode',
                    value: 'http://test.example/fake/other#it',
                },
                predicate: {
                    termType: 'NamedNode',
                    value: 'http://test.example/vocab/predicate',
                },
                subject: {
                    termType: 'NamedNode',
                    value: 'http://test.example/fake/path#me',
                },
                why: {
                    termType: 'NamedNode',
                    value: 'http://test.example/fake/path',
                },
            },
        ]);
    });

    it('should import parent folder as container', () => {
        readFile.mockReturnValue('<> <> <>.');
        const entry = {
            path: 'fake/path',
            fullPath: '/full/fake/path',
        };
        importFromFile(entry, store);
        expect(importContainers).toHaveBeenCalledWith('/fake/path', store);
    });
});
