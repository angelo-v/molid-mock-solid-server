import path from 'path';

const config = {
    rdfBaseUri: `https://molid.example`,
    initialDataDir: path.join(__dirname, '../data'),
    defaultDataDir: path.join(process.cwd(), '.molid'),
};

export const { rdfBaseUri, initialDataDir, defaultDataDir } = config;
