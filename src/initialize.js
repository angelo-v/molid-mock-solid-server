import { initialDataDir } from './config';

const { copy, exists } = require('./file-system');

export default dataDir => {
    if (exists(dataDir)) {
        console.log('using existing data dir', dataDir);
        return Promise.resolve(dataDir);
    }
    console.log('initializing new data dir', dataDir);
    return copy(initialDataDir, dataDir).then(() => dataDir);
};
