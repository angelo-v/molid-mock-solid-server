import express from 'express';
import cors from 'cors';

import { rdfBaseUri } from './config';
import { serialize, sym } from 'rdflib';

export const startServer = async (store, port = 0) => {
    const app = express();

    app.use(
        cors({
            origin: function(origin, callback) {
                if (!origin) return callback(null, true);
                return callback(null, true);
            },
        })
    );

    app.get('*', function(req, res) {
        const doc = sym(rdfBaseUri + req.originalUrl);
        const exists = !!store.any(null, null, null, doc);
        if (exists) {
            res.header('Content-Type', 'text/turtle');
            res.send(serialize(doc, store, null, 'text/turtle'));
        } else {
            res.status(404).send('Not Found');
        }
    });

    const server = await new Promise(resolve => {
        const server = app.listen(port, function() {
            console.log(
                `Molid - mock Solid server listening on port ${
                    this.address().port
                }!`
            );
            resolve(server);
        });
    });

    return {
        stop: () => {
            console.log('Molid - shutting down...');
            return server.close();
        },
        uri: (path = '/') => `http://localhost:${server.address().port}${path}`,
    };
};
