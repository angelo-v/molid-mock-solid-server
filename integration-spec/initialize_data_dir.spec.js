import { existsSync } from 'fs';
import { start } from '../src';
import del from 'del';
import path from 'path';

describe('initialize data dir', () => {
    const testDataDir = path.join(__dirname, '.molid-test');

    describe('given it does not exist yet', () => {
        it(`there is no folder at ${testDataDir}`, () => {
            expect(existsSync(testDataDir)).toBe(false);
        });

        afterAll(() => {
            del.sync([testDataDir]);
        });

        describe(`when molid starts with data dir ${testDataDir}`, () => {
            let molid;
            beforeAll(async () => {
                molid = await start({
                    dataDir: testDataDir,
                });
            });

            afterAll(async () => {
                await molid.stop();
            });

            it(`folder is created at ${testDataDir}`, () => {
                expect(existsSync(testDataDir)).toBe(true);
            });
        });
    });
});
