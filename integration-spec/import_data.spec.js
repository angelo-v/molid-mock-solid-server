import { start } from '../src';
import path from "path";
import { graph, Fetcher, sym, Namespace } from 'rdflib';


const foaf = new Namespace('http://xmlns.com/foaf/0.1/');

describe('import data', () => {

    describe('for first test', () => {

        const firstDataDir = path.join(__dirname, '.first-test');

        let store;
        let fetcher;
        let molid;

        beforeAll(async () => {
            store = graph();
            fetcher = new Fetcher(store);
            molid = await start({
                dataDir: firstDataDir
            });
        });

        afterAll(async () => {
            await molid.stop();
        });

        it('responds with data from .first-test', async () => {
            console.log('first test fetch from ', molid.uri('/profile/card#me'))
            await fetcher.load(molid.uri('/profile/card#me'));
            const name = store.any(sym(molid.uri('/profile/card#me')), foaf("name")).value;
            expect(name).toEqual("First Person");
        });

    });

    describe('for second test', () => {

        const secondDataDir = path.join(__dirname, '.second-test');

        let store;
        let fetcher;
        let molid;

        beforeAll(async () => {
            store = graph();
            fetcher = new Fetcher(store);
            molid = await start({
                dataDir: secondDataDir
            });
        });

        afterAll(async () => {
            await molid.stop();
        });

        it('responds with data from .second-test', async () => {
            console.log('second test fetch from ', molid.uri('/profile/card#me'))
            await fetcher.load(molid.uri('/profile/card#me'));
            const name = store.any(sym(molid.uri('/profile/card#me')), foaf("name")).value;
            expect(name).toEqual("Second Person");
        });

    });

});
