import { givenMolid } from '../src/molid-jest';

describe('server responses', () => {
    givenMolid('with a profile document', molid => {
        it('successfully fetches profile document', async () => {
            const response = await fetch(molid.uri('/profile/card#me'));
            expect(response.status).toEqual(200);
        });

        it('successfully fetches profile container', async () => {
            const response = await fetch(molid.uri('/profile/'));
            expect(response.status).toEqual(200);
        });

        it('responds with 404 for non-existing document', async () => {
            const response = await fetch(molid.uri('/profile/other#me'));
            expect(response.status).toEqual(404);
        });
    });
});
