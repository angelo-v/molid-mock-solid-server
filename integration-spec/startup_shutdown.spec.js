import { start } from '../src';

describe('molid server', () => {

    describe('on defined port', () => {
        let molid;
        beforeAll(async () => {
            molid = await start({ port: 3333 });
        });

        afterAll(async () => {
            await molid.stop();
        });

        it('starts and responds successfully', async () => {
            const response = await fetch('http://localhost:3333/');
            expect(response.status).toBeDefined();
        });

        it('refuses connection after stopping', async () => {
            await molid.stop();
            return expect(fetch('http://localhost:3333/')).rejects.toEqual(
                new Error(
                    'request to http://localhost:3333/ failed, reason: connect ECONNREFUSED 127.0.0.1:3333',
                ),
            );
        });
    });

    describe('several on random port', () => {
        let molid1;
        let molid2;
        beforeAll(async () => {
            molid1 = await start();
            molid2 = await start();
        });

        afterAll(async () => {
            await molid1.stop();
            await molid2.stop();
        });

        it('first server starts and responds successfully', async () => {
            const response = await fetch(molid1.uri());
            expect(response.status).toBeDefined();
        });

        it('second server starts and responds successfully', async () => {
            const response = await fetch(molid2.uri());
            expect(response.status).toBeDefined()
        });

        it('both servers have separate URIs', () => {
            expect(molid1.uri()).not.toEqual(molid2.uri())
        });

    });
});
