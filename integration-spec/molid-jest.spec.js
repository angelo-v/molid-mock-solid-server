import { existsSync } from 'fs';
import path from 'path';
import sanitize from 'sanitize-filename';
import del from 'del';
import { givenMolid } from '../src/molid-jest';

describe('the givenMolid block', () => {
    const testDataDir = path.join(__dirname, sanitize('is used.molid'));

    afterAll(() => {
        del.sync([testDataDir]);
    });

    it(`there is no folder at ${testDataDir}`, () => {
        expect(existsSync(testDataDir)).toBe(false);
    });

    givenMolid('is used', molid => {
        it('the server starts and responds successfully', async () => {
            const response = await fetch(molid.uri());
            expect(response.status).toBeDefined();
        });

        it(`a folder is created at ${testDataDir}`, () => {
            expect(existsSync(testDataDir)).toBe(true);
        });
    });
});
