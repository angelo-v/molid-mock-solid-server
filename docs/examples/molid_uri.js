import { start } from 'molid';

async function your_code() {
    const molid = await start();
    const webId = molid.uri('/person/card#me');
    const response = await fetch(webId);
    // ...
}
