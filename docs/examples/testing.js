import { start } from 'molid';
import { loadProfile } from './your-code';

describe('using Molid for testing', () => {
    let molid;
    beforeAll(async () => {
        molid = await start({
            dataDir: path.join(__dirname, '.mock-profile-john'),
        });
    });

    afterAll(async () => {
        await molid.stop();
    });

    it('successfully fetches the profile document', async () => {
        const webId = molid.uri('/profile/card#me');
        const profile = await loadProfile(webId);
        expect(profile.name).toEqual('John');
    });
});
