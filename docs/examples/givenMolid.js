import { givenMolid } from 'molid/lib/molid-jest';
import { loadProfile } from './your-code';

describe('using the givenMolid function', () => {
    givenMolid('with a profile of a person called John', molid => {
        it('successfully fetches the profile document', async () => {
            const webId = molid.uri('/profile/card#me');
            const profile = await loadProfile(webId);
            expect(profile.name).toEqual('John');
        });
    });
});
