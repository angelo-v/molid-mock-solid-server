import { start } from 'molid';

async function your_code() {
    const molid = await start({
        port: 3456,
        dataDir: path.join(__dirname, '.fake-data'),
    });
}
