import { start } from 'molid';

async function your_code() {
    const molid = await start();
    // do your things ...
    molid.stop();
}
