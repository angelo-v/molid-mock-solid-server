==================
The data directory
==================

Molid uses the file system to initialize the data it provides at runtime. Unlike real Solid server implementations like
`Node Solid Server <https://github.com/solid/node-solid-server>`_ it will never *change* any data in that directory.

Default data dir
~~~~~~~~~~~~~~~~

When you start Molid via CLI or an npm script, it will use the default data dir called `.molid`, relative to the place
where you execute the command. The default data dir is also used when :doc:`calling start() <programmatic-start>` without
a ``dataDir``.

Initialization
~~~~~~~~~~~~~~

When Molid starts it will create the data dir, if it does not yet exist. In that case, it will initialize the directory
with a minimal Solid profile. If the data dir does already exist, Molid will not touch anything.

After initialization, Molid will continue to import the data from that directory into it's internal store.

Data Import
~~~~~~~~~~~

After initialization, Molid imports all data from the data dir to an internal store. The relative path of the files
results in a matching url path, but the file endings starting with ``$`` will be stripped.

Some examples:

==============================  ============================================
  File                            URL path
==============================  ============================================
  .molid/public/bookmarks         /public/bookmarks
  .molid/notes.ttl                /notes.ttl
  .molid/profile/card$.ttl        /profile/card
==============================  ============================================

LDP Containers
---------

Folders that contain at least one imported file, will lead to the creation of an LDP container.
This includes parent folders. For example, if there is a file `.molid/frist/second/file.ttl`,
the following containers will be created:

==============================  ============================================
  Folder                          Container URL path
==============================  ============================================
  .molid/first/second/            /first/second/
  .molid/first/                   /first/
  .molid/                         /
==============================  ============================================

.. note::
    All container URLs end with a `/`. Requests without trailing slash will be answered with 404 Not Found.


Edit data
---------

You can edit, add or remove files inside the data directory as you wish. All files have to contain valid RDF in turtle
format. A folder has to contain at least one imported file (directly or in a sub-folder), otherwise it will be ignored.

.. note::
    You have to restart Molid after you changed data.
