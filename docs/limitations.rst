===========
Limitations
===========

Molid is still in early development. Some features are missing intentionally, since Molid is a mock server and no full
implemention of the Solid specification. Some other features might be useful to mock, but are missing yet.

Things Molid does not provide:

  * Authentication / Authorization
  * Link headers
  * Metadata of container contents (sizes, modification dates, ...)
  * Updating data (POST, PUT, PATCH)
  * Support for any file formats other than turtle
  * for sure some more things not listed here

If one of the limitations are holding you back from using Molid, please file a feature request or contribute by
submitting a merge request ❤