Starting programmatically
=========================

You can start Molid programmtically from inside your application or test:

.. literalinclude:: examples/start.js
   :language: javascript
   :emphasize-lines: 1,4
   :linenos:

The code above will start Molid using a random available port and import data from :doc:`the default data directory <data-dir>`.

Start parameters
----------------

You can pass a configuration object to the ``start`` function:

.. literalinclude:: examples/start_with_config.js
   :language: javascript
   :emphasize-lines: 5,6
   :linenos:


===============  ============================================
  Parameter        Description
===============  ============================================
  port             The port to start Molid on
  dataDir          Absolute path to the data directory
===============  ============================================


Construct an absolute URI
-------------------------

When starting Molid, you probably want to fetch something from it, and therefore need the URI of a document.
When Molid starts on a random port, you will not know the URI. But even if you defined the port yourself,
the following function is still handy to construct absolute URIs referring to Molid resources:

.. literalinclude:: examples/molid_uri.js
   :language: javascript
   :emphasize-lines: 5
   :linenos:


Stopping Molid
--------------

To stop a running Molid instance, call the ``stop`` method on the object returned from ``start``

.. literalinclude:: examples/stop.js
   :language: javascript
   :emphasize-lines: 6
   :linenos:

.. note::
    You can start multiple Molid instances at once on different ports.