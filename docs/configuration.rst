=============
Configuration
=============

Adjust port
--------------

The port Molid is running on can be adjusted by setting the ``PORT`` environment variable to the desired value.

.. code-block:: bash

    PORT=3030 npm run molid
