.. mdinclude:: ../Readme.md

Contents
--------

.. toctree::
   :maxdepth: 2

   installation
   data-dir
   programmatic-start
   testing
   configuration
   limitations
   changelog