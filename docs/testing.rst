Usage in integration tests
==========================

General approach
----------------

By :doc:`starting Molid programmatically <programmatic-start>` it can easily be used inside integration tests. Just
start Molid in a setup method and stop it during tear down.

Here is an example using Jest, but the approach can be applied to any test framework:

.. literalinclude:: examples/testing.js
   :language: javascript
   :emphasize-lines: 7-9,13,17
   :linenos:

In lines 7-9 Molid is started before all tests, using a data directory next to the test. We placed some fake data in
that directory and the expectations in our test are based on the data.
In line 17 we construct a WebID using ``molid.uri()`` and pass it to a
fictional ``loadProfile`` method. Line 13 finally stops the running Molid instance after all tests.

.. warning::
    Be careful to always ``await`` both, the startup and stopping of Molid!

Jest support
------------

Molid has built-in support to simplify `Jest <https://jestjs.io>`_ tests.

givenMolid(name, fn)
~~~~~~~~~~~~~~~~~~~~

You can use ``givenMolid(name, fn)`` to create a Jest ``describe`` block, that will handle startup and shutdown
of a Molid instance for you. The following test is an equivalent to the one described in the section
`General approach`_, but using ``givenMolid``:


.. literalinclude:: examples/givenMolid.js
   :language: javascript
   :emphasize-lines: 1,5
   :linenos:

Molid will start up before *all* of the tests of that group and shutdown afterwards. The running instance is passed
to ``fn``, so that you can easily generate URIs via ``molid.uri()`` inside your tests.

Molid will use a data directory next to the test file, that equals the ``name`` plus the extension ``.molid``.
So in the example above the data directory will be ``with a profile of a person called John.molid``.

.. note::
    Be aware that the directory name may be `sanitized <https://www.npmjs.com/package/sanitize-filename>`_
    when you are using some fancy characters in the ``name``.