============
Installation
============

Install as a global CLI command
-------------------------------

.. code-block:: bash

    $ npm install --global molid

then start Molid anywhere via

.. code-block:: bash

    $ molid


Be aware that the data directory ``.molid`` will be created, where you execute the command.

Install for usage within your Solid app project
-----------------------------------------------

.. code-block:: bash

    $ npm install --save-dev molid


Add a script to your package.json:

.. code-block:: json

    {
      "scripts": {
        "molid": "molid"
      }
    }


Then start Molid via

.. code-block:: bash

    $ npm run molid
